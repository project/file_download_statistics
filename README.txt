CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration

INTRODUCTION
------------
File download statistics ( file download counting)
This module provides statistics for Private and Public fields.
it's provides full integration with views and file format.


 * For a full description of the module visit:
   https://www.drupal.org/project/file_download_statistics

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/file_download_statistics


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

The Advanced help module provides extended documentation. Once enabled,
navigate to Administration > Advanced Help and select the Chaos Tools link to
view documentation.

 * Advanced help - https://www.drupal.org/project/advanced_help


INSTALLATION
------------

 * Install the Chaos Tool Suite module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

No configuration is needed.
